import pygame
import random
from config import *

class Map:
    tiles = []
    def __init__(self, width, height):
        for i in xrange(width):
            self.tiles.append([])
            for j in xrange(height):
                self.tiles[i].append(Tile(self.getRandomType(), i, j))
        
    def getRandomType(self):
        seed = random.random()
        if 0 <= seed < 0.45:
            ID = "DIRT"
        if 0.45 <= seed < 0.65:
            ID = 1
        if 0.65 <= seed < 0.7:
            ID = "MOUNTAIN"
        if 0.7 <= seed < 1:
            ID = "FOREST"
        return ID

    def draw(self, screen):
        for i in xrange(len(self.tiles)):
            for j in xrange(len(self.tiles[i])):
                self.tiles[i][j].draw(screen)

class Tile:
    IDToType = {0: "DIRT", 1: "WATER", 2: "MOUNTAIN", 3: "FOREST"}
    def __init__(self, tileType, x, y):
        tileType = self.transformToString(tileType)
        self.TILE_SIDE = TILE_SIDE
        if tileType == "DIRT":
            self.ID = 0
            self.color = "#00AA00"        
        if tileType == "WATER":
            self.ID = 1
            self.color = "#0000AA"
        if tileType == "MOUNTAIN":
            self.ID = 2
            self.color = "#9d9d9d"
        if tileType == "FOREST":
            self.ID = 3
            self.color = "#003300"

        self.x = x
        self.y = y
        
        self.surface = pygame.Surface((self.TILE_SIDE, self.TILE_SIDE))
        self.surface.fill(pygame.Color(self.color))

    def transformToString(self, tileType):
        if isinstance(tileType, int):
            tileType = self.IDToType[tileType]
        return tileType

    def draw(self, screen):
        screen.blit(self.surface, (self.x * self.TILE_SIDE, self.y * self.TILE_SIDE))
