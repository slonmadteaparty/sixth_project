import pygame
import random
from math import ceil
from config import *

class Unit(pygame.sprite.Sprite):
    MAX_DISTANCE = 0
    IS_FOCUSED = False
    
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.x = x // TILE_SIDE
        self.y = y // TILE_SIDE
        self.image = pygame.Surface((TILE_SIDE, TILE_SIDE))
        self.image.fill(pygame.Color("#000000"))
        self.rect = pygame.Rect(x, y, TILE_SIDE, TILE_SIDE)

    def clickEvent(self, click_x, click_y):
        if self.IS_FOCUSED:
            self.update(ceil(click_x / TILE_SIDE) - self.x, ceil(click_y / TILE_SIDE) - self.y)
            self.IS_FOCUSED = False
        else:
            self.IS_FOCUSED = self.isClicked(click_x, click_y)
    
    def isClicked(self, click_x, click_y):
        flag = True
        flag &= (self.rect.x <= click_x <= self.rect.x + TILE_SIDE)
        flag &= (self.rect.y <= click_y <= self.rect.y + TILE_SIDE)
        return flag

    def update(self, delta_x, delta_y):
        if abs(delta_x) + abs(delta_y) <= self.MAX_DISTANCE:
            self.x += delta_x
            self.y += delta_y
            self.rect.x += delta_x * TILE_SIDE
            self.rect.y += delta_y * TILE_SIDE

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
        if self.IS_FOCUSED:
            screen.blit(pygame.Surface((TILE_SIDE // 2, TILE_SIDE // 2)), (self.rect.x + TILE_SIDE // 4, self.rect.y + TILE_SIDE // 4))

class Scout(Unit):
    MAX_DISTANCE = 5
    IS_FOCUSED = False
    
    def __init__(self, x, y):
        Unit.__init__(self, x, y)
        self.image = pygame.Surface((TILE_SIDE, TILE_SIDE))
        self.image.fill(pygame.Color("#AA0000"))
        self.rect = pygame.Rect(x, y, TILE_SIDE, TILE_SIDE)

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
        if self.IS_FOCUSED:
            screen.blit(pygame.Surface((TILE_SIDE // 2, TILE_SIDE // 2)), (self.rect.x + TILE_SIDE // 4, self.rect.y + TILE_SIDE // 4))

        
class Settler(Unit):
    MAX_DISTANCE = 2
    IS_FOCUSED = False
    
    def __init__(self, x, y):
        Unit.__init__(self, x, y)
        self.image = pygame.Surface((TILE_SIDE, TILE_SIDE))
        self.image.fill(pygame.Color("#111111"))
        self.rect = pygame.Rect(x, y, TILE_SIDE, TILE_SIDE)
            
    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
        if self.IS_FOCUSED:
            screen.blit(pygame.Surface((TILE_SIDE // 2, TILE_SIDE // 2)), (self.rect.x + TILE_SIDE // 4, self.rect.y + TILE_SIDE // 4))
