import pygame
import random
from math import ceil
from units import Settler, Scout
from map import Map
from config import *

# TEMP_CONST
SCOUTS_AMOUNT = 5
SETTELERS_AMOUNT = 5
# =====

def generateRandomUnits():
    units = []
    for i in range(SCOUTS_AMOUNT):
        units.append(Scout(random.randint(0, MAP_TILE_HEIGHT - 1) * TILE_SIDE, random.randint(0, MAP_TILE_WIDTH - 1) * TILE_SIDE))
    for i in range(SETTELERS_AMOUNT):
        units.append(Settler(random.randint(0, MAP_TILE_HEIGHT - 1) * TILE_SIDE, random.randint(0, MAP_TILE_WIDTH - 1) * TILE_SIDE))
    return units

    
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Civilization")
bg = pygame.Surface((SCREEN_WIDTH, SCREEN_HEIGHT))
bg.fill(pygame.Color("#440000"))

map_ = Map(MAP_TILE_WIDTH, MAP_TILE_HEIGHT)

units = generateRandomUnits()

m_loop = True
while m_loop:
    delta_x = 0
    delta_y = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            m_loop = False
        if event.type == pygame.KEYDOWN:
            delta_x, delta_y = 0, 0
            if event.key in [pygame.K_w, pygame.K_UP]:
                delta_x, delta_y = 0, -TILE_SIDE
            elif event.key in [pygame.K_a, pygame.K_LEFT]:
                delta_x, delta_y = -TILE_SIDE, 0
            elif event.key in [pygame.K_s, pygame.K_DOWN]:
                delta_x, delta_y = 0, TILE_SIDE
            elif event.key in [pygame.K_d, pygame.K_RIGHT]:
                delta_x, delta_y = TILE_SIDE, 0
            
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                for unit in units:
                    unit.clickEvent(*event.pos)
                        

    map_.draw(screen)
    for unit in units:
        unit.draw(screen)
    pygame.display.update()
